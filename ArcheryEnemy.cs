﻿using UnityEngine;
using System.Collections;

public class ArcheryEnemy : Enemy {

    private int damage = 0;
    public GameObject aim;
    private Arrow arrow;
    private Object portal;
    public float maxSpeed = 3f;
    public bool facingRight = true;
    public bool moveStatus = true;
    private int moveKoeff = 1;
    public bool shoot;
    private float time;
    public int number = 1;
    private bool invulnerability = false;


    void Awake()
    {
        arrow = Resources.Load<Arrow>("Arrow");
      
    }
    // Use this for initialization
    void Start()
    {

       
    }
    void FixedUpdate()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (shoot)
            if(!GameObject.Find("Arrow" + number))
                    Shoot();

        if (moveStatus)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(maxSpeed, 0f));
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveKoeff * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            Flip();
            moveStatus = true;

        }
        if ((aim.transform.position.x - transform.position.x) > 0)
        {
            facingRight = false;
            Vector3 theScale = transform.localScale;
            theScale.x = -1;
            transform.localScale = theScale;
        }
        else
        {
            facingRight = true;
            Vector3 theScale = transform.localScale;
            theScale.x = 1;
            transform.localScale = theScale;
        }

    }
    private void Shoot()
    {
        float deltaX = aim.transform.position.x - transform.position.x;
        float deltaY = aim.transform.position.y - transform.position.y;
        Quaternion rotation = Quaternion.Euler(0, 0, Mathf.Rad2Deg * Mathf.Atan(deltaY / deltaX));

        Vector3 position = transform.position;
        
        var newArrow = Instantiate(arrow, position, rotation) as Arrow;
        newArrow.gameObject.name = "Arrow" + number;
        newArrow.parent = gameObject;
        if (facingRight)
            newArrow.direction = 1f;
        else newArrow.direction = -1f;
        newArrow.destroyTime = 2f;
        newArrow.x = aim.transform.position.y - transform.position.y;
        newArrow.y = aim.transform.position.x - transform.position.x;

    }
    public override void recieveDamage()
    {
        if ((Time.time - time) < 0.1f)
            invulnerability = true;
        else invulnerability = false;
        if (!invulnerability)
            damage++;
        time = Time.time;
        if (damage == 2)
            die();

    }
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (unit && unit is characterController)
        {
            if (Mathf.Abs(unit.transform.position.y - transform.position.y) > 1.2f && Mathf.Abs(unit.transform.position.x - transform.position.x) < GetComponent<Renderer>().bounds.size.y)
            {
                unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(2000, 1000), ForceMode2D.Force);
                recieveDamage();
            }
            else
            {
                unit.recieveDamage();
                if ((unit.transform.position.x - transform.position.x) < 0)
                    unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
                else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);

            }
        }
        if (laser && laser.parent != gameObject)
            recieveDamage();
        if ((collider.gameObject.name == "stopCollaiderLeft"))
        {
            moveStatus = false;
            moveKoeff = -1;
        }
        if ((collider.gameObject.name == "stopCollaiderRight"))
        {
            moveStatus = false;
            moveKoeff = 1;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
