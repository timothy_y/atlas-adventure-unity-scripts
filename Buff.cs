﻿using UnityEngine;
using System.Collections;

public class Buff : MonoBehaviour {

    public bool timeBuff = false;
    public bool healthBuff = false;
    public bool energyBuff = false;
    public bool atomicBuff = false;
    private float buffTime;
    characterController hero;

    // Use this for initialization
    void Start () {
        
	}

    // Update is called once per frame
    void Update()
    {
        if (hero)
        {
            if ((Time.unscaledTime - buffTime) > 10f)
            {
                if (timeBuff)
                    Time.timeScale = 1f;
                else if (energyBuff)
                {
                    hero.energyReloadTime = 3f;
                    hero.Energy.maxValue = 3f;
                    
                }
                else if (atomicBuff)
                {
                    hero.energyReloadTime = 3f;
                    hero.Energy.maxValue = 3f;
                    Time.timeScale = 1f;
                }
                DestroyObject(gameObject);
            }

        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        hero = collider.GetComponent<characterController>();
        if (hero)
        {
            buffTime = Time.unscaledTime;
            if (timeBuff)
                Time.timeScale = 0.5f;
            else if (healthBuff)
            {
                if (hero.lives != 3)
                {
                    hero.lives++;
                    hero.livesBar.value++;
                }
            }
            else if (energyBuff)
            {
                hero.energyReloadTime = 0.4f;
                hero.Energy.maxValue = 0.4f;
            }
            else if (atomicBuff)
            {
                hero.energyReloadTime = 0.2f;
                hero.Energy.maxValue = 0.3f;
                if (hero.lives != 3)
                {
                    hero.lives++;
                    hero.livesBar.value++;
                }
                Time.timeScale = 1.5f;
                Debug.Log("bOOOOOOm!!!");
            }
            gameObject.GetComponent<Renderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
        

    }
}
