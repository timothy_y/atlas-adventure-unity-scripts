﻿using UnityEngine;
using System.Collections;

public abstract class Enemy : Unit
{
    
    protected virtual void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (unit && unit is characterController)
            unit.recieveDamage();
       
    }
    
}
