﻿using UnityEngine;
using System.Collections;

public abstract class Unit : MonoBehaviour {

	public virtual void recieveDamage()
    {
        die();
        
    }
    public virtual void die()
    {
        Destroy(gameObject);
    }
}
