﻿using UnityEngine;
using System.Collections;

public class DominoWallAnimation : MonoBehaviour {

    public float animtionTime;
    public int animationIndex;
    public bool firstAnimation = false;
    private float awakeTime;

	// Use this for initialization
	void Start () {
        awakeTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if(!firstAnimation)
            if ((Time.time - awakeTime) > animationIndex * animtionTime)
                GetComponent<Animator>().enabled = true;
	}
}
