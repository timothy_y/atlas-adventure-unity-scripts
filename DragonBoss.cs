﻿using UnityEngine;
using System.Collections;

public class DragonBoss : Enemy {
    private int damage = 0;
    public float rate = 2.0f;
    public GameObject aim;
    private Rocket rocket;
    private Object portal;
    public float maxSpeed = 3f;
    public bool facingRight = true;
    public bool moveStatus = true;
    private int moveKoeff = 1;


    void Awake()
    {
        rocket = Resources.Load<Rocket>("DragonFire");
        
    }
    // Use this for initialization
    void Start()
    {

        InvokeRepeating("Shoot", rate, rate);
    }

    // Update is called once per frame
    void Update()
    {
        if (moveStatus)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(maxSpeed, 0f));
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveKoeff * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            Flip();
            moveStatus = true;

        }
        if (Mathf.Abs(aim.transform.position.x - gameObject.transform.position.x) < 35f)
        {
            Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
            cam.orthographicSize = 11f;
        }
        if ((aim.transform.position.x - transform.position.x) < 0)
        {
            facingRight = false;
            Vector3 theScale = transform.localScale;
            theScale.x = -1;
            transform.localScale = theScale;
        }
        else
        {
            facingRight = true;
            Vector3 theScale = transform.localScale;
            theScale.x = 1;
            transform.localScale = theScale;
        }

    }
    public override void die()
    {
        gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
        DestroyObject(gameObject, 0.5f);
    }
    public override void recieveDamage()
    {
        damage++;
        if (damage == 6)
        {
            die();
            GameObject.Find("ExitPlatform").GetComponent<FlyPlatformInSpace>().maxSpeed = 5f;
            GameObject.Find("Main Camera").GetComponent<Camera>().orthographicSize = 5f;
        }
    }
    private void Shoot()
    {
        float deltaX = aim.transform.position.x - transform.position.x;
        float deltaY = aim.transform.position.y - transform.position.y - 2f;
        Quaternion rotation = Quaternion.Euler(0, 0, Mathf.Rad2Deg * Mathf.Atan(deltaY / deltaX));

        Vector3 position = transform.position;
        position.y += 2f;
        var newRocket = Instantiate(rocket, position, rotation) as Rocket;
        newRocket.parent = gameObject;
        if (facingRight)
            newRocket.direction = 1f;
        else newRocket.direction = -1f;
        newRocket.destroyTime = 3f;
        newRocket.x = aim.transform.position.y - transform.position.y - 2f;
        newRocket.y = aim.transform.position.x - transform.position.x;

    }
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (unit && unit is characterController)
        {
            unit.recieveDamage();
            if ((unit.transform.position.x - transform.position.x) < 0)
                unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
            else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);

        }
        if (laser)
            recieveDamage();

        if ((collider.gameObject.name == "stopCollaiderLeft"))
        {
            moveStatus = false;
            moveKoeff = -1;
        }
        if ((collider.gameObject.name == "stopCollaiderRight"))
        {
            moveStatus = false;
            moveKoeff = 1;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}


