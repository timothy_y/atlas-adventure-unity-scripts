﻿using UnityEngine;
using System.Collections;


public class TextScript : MonoBehaviour {

    public int score { set; get; }
    public int MaxScore { get; set; }
    public float levelTime { get; set; }
    public bool forLevelCompleteMenu = false;
    public bool forTimeField = false;
    public bool forScorePanel = true;
    
    UnityEngine.UI.Text text;

	// Use this for initialization
	void Start () {
	score = 69;
	}
	
	// Update is called once per frame
	void Update () {
       text = gameObject.GetComponent<UnityEngine.UI.Text>();
        if (forScorePanel)
            text.text = System.Convert.ToString(score);
        else if (forLevelCompleteMenu)
            text.text = System.Convert.ToString(score) + " \\ " + MaxScore;
        else if (forTimeField)
            text.text = System.Convert.ToString(levelTime) + "  s";
    }


}
