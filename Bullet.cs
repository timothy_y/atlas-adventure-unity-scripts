﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public float speed = 10f;
    public float destroyTime { set; get; }
    public float direction { set; get; }
    public GameObject parent { set; get; }
    public GameObject Field { set; get; }
    private SpriteRenderer sprite;
    private void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
        destroyTime = 1.5f;
    }
    private void Start()
    {
        Destroy(gameObject, destroyTime);

    }
    void Update()
    {
        Vector3 offset = transform.position;
        Vector3 theScale = transform.localScale;
        offset.x = transform.position.x* direction;
        offset.y = 0f;
        transform.position = Vector3.MoveTowards(transform.position, transform.position + offset, speed * Time.deltaTime);

    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        if (unit && unit.gameObject != parent && unit.gameObject != Field)
        {
            Destroy(gameObject);
            unit.recieveDamage();
        }
    }
}
