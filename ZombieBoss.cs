﻿using UnityEngine;
using System.Collections;

public class ZombieBoss : Enemy {

    public int damage = 0;
    private float maxSpeed = 0f;
    bool facingRight = true;
    public bool moveStatus = true;
    private int moveKoeff = 1;
    public bool startMove = false;

    // Use this for initialization
    void Start()
    {

    }
    void FixedUpdate()
    {
       
    }
    // Update is called once per frame
    void Update()
    {
        if (!startMove)
        { 
            if (Mathf.Abs(GameObject.Find("hero").transform.position.x - gameObject.transform.position.x) < 7f)
            {
                Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
                startMove = true;
                gameObject.GetComponent<Animator>().enabled = true;
                cam.orthographicSize = 10f;
                maxSpeed = 3f;
            }
        }
       
            if (moveStatus)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(maxSpeed, 0f));
                GetComponent<Rigidbody2D>().velocity = new Vector2(moveKoeff * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                Flip();
                moveStatus = true;

            }
       
    }
  
    public override void recieveDamage()
    {
        damage++;
        if (damage == 5)
        {
            GameObject.Find("spaceRocket").GetComponent<BoxCollider2D>().isTrigger = true;
            DestroyObject(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (unit && unit is characterController)
        {
            unit.recieveDamage();
            if ((unit.transform.position.x - transform.position.x) < 0)
                unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
            else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);

        }
        if (laser)
            recieveDamage();

        if ((collider.gameObject.name == "stopCollaiderLeft"))
        {
            moveStatus = false;
            moveKoeff = -1;
        }
        if ((collider.gameObject.name == "stopCollaiderRight"))
        {
            moveStatus = false;
            moveKoeff = 1;
        }
    }
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
