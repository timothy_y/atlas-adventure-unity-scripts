﻿using UnityEngine;
using System.Collections;

public class LongLaser : MonoBehaviour {
    public float deltaX {get; set;}
    public float deltaY { get; set; }
    public float time { get; set; }
    public bool direction { set; get; }
    public GameObject parent { set; get; }
    private SpriteRenderer sprite;
    private characterController hero;
    private void Awake()
    {
        hero = GameObject.Find("hero").GetComponent<characterController>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        
    }
    
    // Use this for initialization
    void Start () {
        Destroy(gameObject, 2f);
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 theScale = transform.localScale;
        if (direction)
            sprite.flipX = false;
        else sprite.flipX = true;
        transform.localScale = theScale;
        var k = (Time.time - time)*10f;
        
        transform.localScale = new Vector3(Mathf.Sqrt(Mathf.Pow(deltaX, 2) + Mathf.Pow(deltaY, 2)) * k, 3, 1);
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        characterController hero = collider.GetComponent<characterController>();
        if (hero)
        {
            hero.recieveDamage();
            if ((hero.transform.position.x - transform.position.x) < 0)
                hero.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
            else hero.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);
        }
    }
}
