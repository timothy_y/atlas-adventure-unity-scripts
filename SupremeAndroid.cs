﻿using UnityEngine;
using System.Collections;

public class SupremeAndroid : MonoBehaviour {

    private GameObject hero;
    private bool written = false;
	// Use this for initialization
	void Start () {
        hero = GameObject.Find("hero");
	}
	
	// Update is called once per frame
	void Update () {

	if((gameObject.transform.position.x -  hero.transform.position.x) < 5f && !written)
        {
            hero.GetComponent<characterController>().maxSpeed = 0f;
            hero.GetComponent<characterController>().jumpForce = 0f;
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Supreme Android: Hello and welcome to the great planet of all robots of the universe.", 0f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Supreme Android:  So what's your name and tell me where are you from?", 3f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Atlas: My name is Atlas, and I'm from planet Earth.", 7f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Supreme Android: Hmm, what a haughty name for planet. Anyway how was your trip?", 10f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Atlas: Not so good, as I wanted, but still not bad!", 14f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Supreme Android: That is great. Now I think I can declare you as a citizen of our planet. Good luck, Atlas!", 17f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("", 22f));
            written = true;
            Invoke("END", 25f);
            Invoke("loadHome", 30f);
        }

	}
    private void END()
    {
        Destroy(GameObject.Find("Canvas"));
        Instantiate(Resources.Load("Author"));
    }
    private void loadHome()
    {
        GameObject.Find("Bechevior").GetComponent<GUI>().loadGUI(0);
    }
}
