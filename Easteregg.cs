﻿using UnityEngine;
using System.Collections;

public class Easteregg : MonoBehaviour {
    public float maxSpeed = 10;
    public bool moveStatus = true;
    private Collider2D col;
    private int moveKoeff = 1;
    public float move;
    public bool start = false;
    void Start()
    {
        
    }

    void Update()
    {
        if (start)
        {
            if (moveStatus)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, maxSpeed));
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, moveKoeff * maxSpeed);
            }
            else
            {
                moveStatus = true;

            }
        }



    }
    protected void OnTriggerEnter2D(Collider2D collider)
    {
        characterController hero = collider.GetComponent<characterController>();
        if ((collider.gameObject.name == "stopCollaiderUp"))
        {
            moveStatus = false;
            moveKoeff = -1;
        }
        if ((collider.gameObject.name == "stopCollaiderDown"))
        {
            moveStatus = false;
            moveKoeff = 1;
        }
        if (hero)
            start = true;
    }



}
