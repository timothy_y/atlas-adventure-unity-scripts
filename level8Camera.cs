﻿using UnityEngine;
using System.Collections;

public class level8Camera : MonoBehaviour {

    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    public Vector3 position;

    // Update is called once per frame
    void Update()
    {
            Vector3 point = GetComponent<Camera>().WorldToViewportPoint(new Vector3(position.x, position.y + 0.75f, position.z));
            Vector3 delta = new Vector3(position.x, position.y + 0.75f, position.z) - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
    }
}
