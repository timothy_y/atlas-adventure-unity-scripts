﻿using UnityEngine;
using System.Collections;

public class RobotBuilding : MonoBehaviour
{
    public bool exit,
        enter = true;

    private GameObject cam,
        hero;
    private bool alreadyIn = false;
    // Use this for initialization
    void Start()
    {
        hero = GameObject.Find("hero");
        cam = GameObject.Find("Main Camera");
    }

    // Update is called once per frame
    void Update()
    {
        if (!alreadyIn)
        {
            if (gameObject.transform.position.x - hero.transform.position.x < 15f)
            {
                Vector3 newPosition = hero.transform.position;
                newPosition.y += 3f;
                cam.GetComponent<level8Camera>().position = newPosition;
            }
            else cam.GetComponent<level8Camera>().position = hero.transform.position;
        }
        if(alreadyIn)
            cam.GetComponent<level8Camera>().position = hero.transform.position;
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject == hero && enter)
        {
            hero.transform.position = GameObject.Find("in").transform.position;
            alreadyIn = true;
        }    
        else if (collider.gameObject == hero && exit)
        {
            hero.transform.position = GameObject.Find("out").transform.position;
            alreadyIn = false;
        }
    }
}