﻿using UnityEngine;
using System.Collections;

public class BossField : Unit {

    private int damage = 0;
    public GameObject target;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
       
        transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z - 2);
	}
    public override void recieveDamage()
    {
        damage++;
        if (damage == 4)
            die();
       
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (unit && unit is characterController)
        {
            unit.recieveDamage();
            if ((unit.transform.position.x - transform.position.x) < 0)
                unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
            else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);

        }
        if (laser && laser.parent != target)
            recieveDamage();
    }
}

