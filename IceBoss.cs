﻿using UnityEngine;
using System.Collections;

public class IceBoss : Enemy {
  public float maxSpeed = 3f;
    bool facingRight = true;
    private Laser laser;
    public bool moveStatus = true;
    private int moveKoeff = 1;
    private int damage = 0;   
    private bool invulnerability = false;
    public bool shoot;
    private float time;

    private void Awake()
    {
        laser = Resources.Load<Laser>("Icicle");
    }
    // Use this for initialization
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(GameObject.Find("hero").transform.position.x - gameObject.transform.position.x) < 40f)
        {
            Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
            cam.orthographicSize = 10f;
        }

        if ((GameObject.Find("hero").transform.position.x - transform.position.x) > 0)
        {
            facingRight = false;
            Vector3 theScale = transform.localScale;
            theScale.x = -Mathf.Abs(transform.localScale.x);
            transform.localScale = theScale;
        }
        else
        {
            facingRight = true;
            Vector3 theScale = transform.localScale;
            theScale.x = Mathf.Abs(transform.localScale.x);
            transform.localScale = theScale;
        }
    
        if (moveStatus)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(maxSpeed, 0f));
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveKoeff * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            Flip();

            moveStatus = true;


        }
        if (shoot)
            if(!GameObject.Find("Icicle(Clone)"))
                Shoot();
      

    }

    public override void recieveDamage()
    {
        damage++;
        if (damage == 6)
        {
            die();
            GameObject.Find("Main Camera").GetComponent<Camera>().orthographicSize = 5f;
        }
           


    }
    private void Shoot()
    {
        Vector3 position = transform.position;
        //position.x += 6f;
        Laser newLaser = Instantiate(laser, position, laser.transform.rotation) as Laser;
        if (facingRight)
            newLaser.direction = 1f;
        else newLaser.direction = -1f;
        newLaser.speed = -12f;
        newLaser.parent = gameObject;
        newLaser.destroyTime = 1.5f;
        newLaser.GetComponent<Rigidbody2D>().isKinematic = true;   
    }
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (unit && unit is characterController)
        { 
                unit.recieveDamage();
                if ((unit.transform.position.x - transform.position.x) < 0)
                    unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
                else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);
        }
        if (laser && laser.parent != gameObject)
            recieveDamage();


        if ((collider.gameObject.name == "stopCollaiderLeft"))
        {
            moveStatus = false;
            moveKoeff = -1;
        }
        if ((collider.gameObject.name == "stopCollaiderRight"))
        {
            moveStatus = false;
            moveKoeff = 1;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
