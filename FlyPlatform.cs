﻿using UnityEngine;
using System.Collections;

public class FlyPlatform : MonoBehaviour {

    public float maxSpeed { set; get; }
    public bool moveStatus = true;
    private int moveKoeff = 1;

    void Start()
    {
        maxSpeed = 0f;
    }

    void Update()
    {
        if (moveStatus)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(maxSpeed, 0f));
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveKoeff * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            moveStatus = true;

        }



    }
    private  void OnTriggerEnter2D(Collider2D collider)
    {
        if ((collider.gameObject.name == "stopCollaiderLeft"))
        {
            moveStatus = false;
            moveKoeff = -1;
        }
        if ((collider.gameObject.name == "stopCollaiderRight"))
        {
            moveStatus = false;
            moveKoeff = 1;
        }
    }



}
