﻿using UnityEngine;
using System.Collections;

public class ShootableFlower : Unit {

    private Laser PeaBullet;
    public float rate = 1f;
    public bool shoot = false;
    private float time;
    // Use this for initialization
    void Start()
    {
        PeaBullet = Resources.Load<Laser>("PeaBullet");
    }
    void Update()
    {
        if (shoot)
            Shoot();
    }

    public override void recieveDamage()
    {
        DestroyObject(gameObject);
    }
    private void Shoot()
    {
        if (Time.time - time > 0.1f)
        {
            time = Time.time;
            Vector3 position = transform.position;
            Laser newLaser = Instantiate(PeaBullet, position, gameObject.transform.rotation) as Laser;
            newLaser.direction = 1f;
            newLaser.parent = gameObject;
            newLaser.destroyTime = 1f;
        }
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (laser && laser.parent != gameObject)
            recieveDamage();
       
        if (unit && unit is characterController)
        {
            unit.recieveDamage();
            if ((unit.transform.position.x - transform.position.x) < 0)
                unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
            else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);

        }
     
    }
 }


