﻿using UnityEngine;
using System.Collections;

public class SubtitleTrigger : MonoBehaviour {
    public string message;
    private bool written = false;
    public float invokeTime = 0f;
    public float timeAfter = 2f;
	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	
	}
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.GetComponent<characterController>() && !written)
        {
            GameSubtitles subtitles = GameObject.Find("Subtitles").GetComponent<GameSubtitles>();
            StartCoroutine(subtitles.printSubtitle(message, invokeTime));
            StartCoroutine(subtitles.printSubtitle("", timeAfter));
            written = true;
        }
    }
}
