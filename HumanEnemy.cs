﻿using UnityEngine;
using System.Collections;

public class HumanEnemy : Enemy {

    public float maxSpeed = 10f;
    bool facingRight = true;
    public float rate = 1.0f;
    private Laser bullet;
    public bool moveStatus = true;
    private int moveKoeff = 1;
    public float move;
    private void Awake()
    {
        bullet = Resources.Load<Laser>("bullet2");
    }
    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Shoot", rate, rate);
    }
    // Update is called once per frame
    void Update()
    {

        if (moveStatus)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(maxSpeed, 0f));
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveKoeff * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            Flip();

            moveStatus = true;


        }



    }
    private void Shoot()
    {
        Vector3 position = transform.position;
        //position.x += 6f;
        Laser newLaser = Instantiate(bullet, position, gameObject.transform.rotation) as Laser;
        if (facingRight)
            newLaser.direction = 1f;
        else newLaser.direction = -1f;
        newLaser.parent = gameObject;
        newLaser.destroyTime = 0.4f;

    }
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (unit && unit is characterController)
        {
            if (Mathf.Abs(unit.transform.position.y - transform.position.y) > 1.2f && Mathf.Abs(unit.transform.position.x - transform.position.x) < GetComponent<Renderer>().bounds.size.y)
                recieveDamage();
            else
            {
                unit.recieveDamage();
                if ((unit.transform.position.x - transform.position.x) < 0)
                    unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
                else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);

            }
        }
        if (laser && laser.parent != gameObject)
            recieveDamage();



        if ((collider.gameObject.name == "stopCollaiderLeft"))
        {
            moveStatus = false;
            moveKoeff = -1;
        }
        if ((collider.gameObject.name == "stopCollaiderRight"))
        {
            moveStatus = false;
            moveKoeff = 1;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
