﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour
{

    public float speed = 10f;
    public float destroyTime { set; get; }
    public float direction { set; get; }
    public GameObject parent { set; get; }
    public SpriteRenderer sprite;
    private characterController hero;
    private void Awake()
    {
        hero = GameObject.Find("hero").GetComponent<characterController>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        destroyTime = 1.5f;
    }
    private void Start()
    {
        Destroy(gameObject, destroyTime);

    }
    void Update()
    {
        
        Vector3 offset = transform.position;
        Vector3 theScale = transform.localScale;
        if(direction < 0)
            theScale.x = -Mathf.Abs(theScale.x);
        else theScale.x = Mathf.Abs(theScale.x);
        offset.x = 200.0f * direction;
        offset.y = 0f;
        
        transform.localScale = theScale;
        transform.position = Vector3.MoveTowards(transform.position, transform.position + offset, speed * Time.deltaTime);
        
    }
	void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit  = collider.GetComponent<Unit>();
        hero = parent.GetComponent<characterController>();
        PowerShield shield = collider.GetComponent<PowerShield>();
        if (unit && unit.gameObject != parent && !hero)
        {
            Destroy(gameObject);
            unit.recieveDamage();
            if ((unit.transform.position.x - transform.position.x) < 0)
                unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
            else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);
        }
        else if (unit && unit.gameObject != parent)
            Destroy(gameObject);
        else if (shield && shield.target.gameObject != parent)
            Destroy(gameObject);

    }
}
