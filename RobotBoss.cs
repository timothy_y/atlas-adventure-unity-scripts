﻿using UnityEngine;
using System.Collections;

public class RobotBoss : Unit {

    private int damage = 0;
    public float rate = 2.0f;
    public GameObject aim;
    private Bullet bullet;
    bool facingRight = true;
    public GameObject field;
    private Object portal;
    private GameObject door;

    void Awake() {
        bullet = Resources.Load<Bullet>("Bullet");
        door = GameObject.Find("ExitDoor");

    }
    // Use this for initialization
    void Start () {
       
        InvokeRepeating("Shoot", rate, rate);
    }
	
	// Update is called once per frame
	void Update () {
        if ((aim.transform.position.x - transform.position.x) > 0)
        {
            facingRight = false;
            Vector3 theScale = transform.localScale;
            theScale.x = -Mathf.Abs(transform.localScale.x);
            transform.localScale = theScale;
        }
        else
        {
            facingRight = true;
            Vector3 theScale = transform.localScale;
            theScale.x = Mathf.Abs(transform.localScale.x);
            transform.localScale = theScale;
        }
    }
    public override void die()
    {
        GetComponent<Rigidbody2D>().isKinematic = false;
        BoxCollider2D[] colliders = GetComponents<BoxCollider2D>();
        for (int i = 0; i < colliders.Length; i++)
            colliders[i].isTrigger = false;
        transform.rotation = Quaternion.Euler(0, 0, -60);
        DestroyObject(gameObject, 1f);
    }

    public override void recieveDamage()
    {
        damage++;
        if (damage == 5)
        {
            door.GetComponent<Animator>().enabled = true;
            door.GetComponent<BoxCollider2D>().isTrigger = true;
            die();
            
        }
    }
    private void Shoot()
    {
        Vector3 position = transform.position;
        var newLaser = Instantiate(bullet, position, gameObject.transform.rotation) as Bullet;
        newLaser.parent = gameObject;
        if (facingRight)
            newLaser.direction = 1f;
        else newLaser.direction = -1f;
        newLaser.destroyTime = 2f;
        newLaser.Field = field;
        
   }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (unit && unit is characterController)
        {
            unit.recieveDamage();
            if ((unit.transform.position.x - transform.position.x) < 0)
                unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
            else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);

        }
        if (laser)
            recieveDamage();

    }
}
