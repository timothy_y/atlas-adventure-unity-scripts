﻿using UnityEngine;
using System.Collections;

public class FireExplosion : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Destroy(gameObject, 1.3f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        if (unit && unit is characterController)
        {
            unit.recieveDamage();
        }
    }
}
