﻿using UnityEngine;
using System.Collections;

public class Сivilians : MonoBehaviour {

    public float maxSpeed = 10f;
    bool facingRight = true;
    public bool moveStatus = true;
    private int moveKoeff = 1;
    public bool kind = false;
    private bool written = false;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (moveStatus)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(maxSpeed, 0f));
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveKoeff * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            Flip();
            moveStatus = true;

        }



    }
    private characterController Hero;
    private void OnTriggerEnter2D(Collider2D collider)
    {
        characterController hero = collider.GetComponent<characterController>();
        Hero = hero;
        if ((collider.gameObject.name == "stopCollaiderLeft"))
        {
            moveStatus = false;
            if (maxSpeed < 0)
                moveKoeff = -1;
            else moveKoeff = 1;
        }
        if ((collider.gameObject.name == "stopCollaiderRight"))
        {
            moveStatus = false;
            if (maxSpeed < 0)
                moveKoeff = 1;
            else moveKoeff = -1;
        }
        if(hero && kind && !written)
        {
            maxSpeed = 0f;
            hero.GetComponent<characterController>().maxSpeed = 0f;
            hero.GetComponent<characterController>().jumpForce = 0f;
            GetComponent<Animator>().enabled = false;
            Invoke("continueMove", 11f);
            
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Atlas: I'm sorry can you tell Where can I find the Intergalactic phone here.", 0.5f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Some robot: You probably crazy or what?", 3f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Some robot: But you should visit the palace of 8 Motherboard.", 5.5f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Some robot: Everyone new must go there and talk with supreme Android.", 8.5f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Atlas: Ok.", 11f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("Atlas: Hmm, The Palace of-8 Motherboard? This place is better and better with time!", 14f));
            StartCoroutine(GameObject.Find("Subtitles").GetComponent<GameSubtitles>().printSubtitle("", 18f));
            written = true;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
     void continueMove()
    {
        GameObject.Find("hero").GetComponent<characterController>().maxSpeed = 8f;
        GameObject.Find("hero").GetComponent<characterController>().jumpForce = 460f;
        GetComponent<Animator>().enabled = true;
        maxSpeed = -3f;
    }
}
