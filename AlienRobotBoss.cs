﻿using UnityEngine;
using System.Collections;

public class AlienRobotBoss : Enemy
{
    private int damage = 0;
    public float rate = 2.0f;
    public GameObject aim;
    private Bullet bullet;
    public GameObject field;
    private Door door;
    public float maxSpeed = 3f;
    bool facingRight = true;
    public bool moveStatus = true;
    private int moveKoeff = 1;
    public float move;

    void Awake()
    {
        bullet = Resources.Load<Bullet>("AlienImpulse");
        door = GameObject.Find("ControlRoomDoor").GetComponent<Door>();
    }
    // Use this for initialization
    void Start()
    {

        InvokeRepeating("Shoot", rate, rate);
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(aim.transform.position.x - gameObject.transform.position.x) < 25f)
        {
            Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
            cam.orthographicSize = 10f; 
        }
        if (moveStatus)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(maxSpeed, 0f));
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveKoeff * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            Flip();
            moveStatus = true;

        }
        if((aim.transform.position.x - transform.position.x) > 0)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x = -1;
            transform.localScale = theScale;
        }
        else
        {
            facingRight = true;
            Vector3 theScale = transform.localScale;
            theScale.x = 1;
            transform.localScale = theScale;
        }

    }
    public override void die()
    {
        GetComponent<Rigidbody2D>().isKinematic = false;
        BoxCollider2D[] colliders = GetComponents<BoxCollider2D>();
        for (int i = 0; i < colliders.Length; i++)
            colliders[i].isTrigger = false;
        transform.rotation = Quaternion.Euler(0, 0, -60);
        DestroyObject(gameObject, 1f);
    }
    public override void recieveDamage()
    {
        damage++;
        if (damage == 7)
        {
            door.GetComponent<Animator>().enabled = true;
            door.inDoor = true;
            die();
            GameSubtitles subtitles = GameObject.Find("Subtitles").GetComponent<GameSubtitles>();
            StartCoroutine(subtitles.printSubtitle("Yeah, i'm  just kick his metal ass!", 1f));
            StartCoroutine(subtitles.printSubtitle("", 3f));

        }
    }
    private void Shoot()
    {
        if (Mathf.Abs(transform.position.y - aim.transform.position.y) < 10f)
        {
            Vector3 position = transform.position;
            position.y = aim.transform.position.y + 0.5f;
            var newLaser = Instantiate(bullet, position, gameObject.transform.rotation) as Bullet;
            newLaser.parent = gameObject;
            if (facingRight)
                newLaser.direction = 1f;
            else newLaser.direction = -1f;
            newLaser.destroyTime = 3f;
            newLaser.Field = field;
        }
    }
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (unit && unit is characterController)
        {
            unit.recieveDamage();
            if ((unit.transform.position.x - transform.position.x) < 0)
                unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
            else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);

        }
        if (laser)
            recieveDamage();

        if ((collider.gameObject.name == "stopCollaiderLeft"))
        {
            moveStatus = false;
            moveKoeff = -1;
        }
        if ((collider.gameObject.name == "stopCollaiderRight"))
        {
            moveStatus = false;
            moveKoeff = 1;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}

