﻿using UnityEngine;
using System.Collections;

public class PowerShield : MonoBehaviour {
    public Transform target;
   
   
    void Update()
    {
        if (target)
        {
            transform.position = target.position;
        }
    }
    public void OnTriggerEnter2D(Collider2D collider)
    {
        characterController hero = collider.GetComponent<characterController>();
        Laser laser = collider.GetComponent<Laser>();
        if (hero) {
            hero.recieveDamage();
            if ((hero.transform.position.x - transform.position.x) < 0)
                hero.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
            else hero.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);
        }
        if (laser && laser.parent != target.gameObject) {
            DestroyObject(gameObject);
            
        }
            
    }
}
