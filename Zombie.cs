﻿using UnityEngine;
using System.Collections;

public class Zombie : Enemy {

    public float maxSpeed = 3f;
    bool facingRight = true;
    public bool moveStatus = true;
    private Collider2D col;
    public bool startMove = false;
    private int moveKoeff = 1;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!startMove)
        {
            if (Mathf.Abs(GameObject.Find("hero").transform.position.x - gameObject.transform.position.x) < 6f)
                startMove = true;
        }

        if (startMove)
        {
            if (moveStatus)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(maxSpeed, 0f));
                GetComponent<Rigidbody2D>().velocity = new Vector2(moveKoeff * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                Flip();
                moveStatus = true;

            }
        }



    }
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        InfectedHuman human = collider.GetComponent<InfectedHuman>();
        if (unit && unit is characterController)
        {
            if (Mathf.Abs(unit.transform.position.y - transform.position.y) > 1.2f && Mathf.Abs(unit.transform.position.x - transform.position.x) < GetComponent<Renderer>().bounds.size.y)
                recieveDamage();
            else
            {
                unit.recieveDamage();
                if ((unit.transform.position.x - transform.position.x) < 0)
                    unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
                else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);

            }

        }
        if (laser)
            recieveDamage();

        if (human)
        {
            human.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Zombie1");
            human.gameObject.AddComponent<Zombie>();
            human.GetComponent<Zombie>().startMove = true;
            human.GetComponent<Zombie>().maxSpeed = 4f;
            human.GetComponent<AudioSource>().enabled = true;
        }

        if ((collider.gameObject.name == "stopCollaiderLeft"))
        {
            moveStatus = false;
            moveKoeff = -1;
        }
        if ((collider.gameObject.name == "stopCollaiderRight"))
        {
            moveStatus = false;
            moveKoeff = 1;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

}
