﻿using UnityEngine;
using System.Collections;
using System.Text;
using UnityEngine.UI;

public class GUI : MonoBehaviour
{

    public Canvas pauseMenu,
        optionsMenu,
        shopMenu;
    public Object newPauseMenu;
    private AudioSource backgroundMusic;

    private AsyncOperation async;
    private Slider loadingBar;
    private Canvas loadingScreen;
    private Slider GFX,
        SFX,
        musicSlider;
    private Toggle musicToggle,
        soundToggle;
    public Button[] levelButtons;
    public Text money;
    public void Awake()
    {
        if (PlayerPrefs.GetInt("FirstLaunch") == 0)
        {
            PlayerPrefs.SetInt("GFXQuality", 5);
            PlayerPrefs.SetFloat("ArmorR", 255f);
            PlayerPrefs.SetFloat("ArmorG", 255f);
            PlayerPrefs.SetFloat("ArmorB", 255f);
            PlayerPrefs.SetFloat("BulletColorR", 255f);
            PlayerPrefs.SetFloat("BulletColorG", 255f);
            PlayerPrefs.SetFloat("BulletColorB", 255f);
            PlayerPrefs.SetInt("FirstLaunch", 1);
        }
        ////////////////////////MUSIC/////////////////////////
        if (!GameObject.Find("GameMusic(Clone)"))
            Instantiate(Resources.Load("GameMusic"));
        backgroundMusic = GameObject.Find("GameMusic(Clone)").GetComponent<AudioSource>();
        ///////////////////////LEVEL LOCK/////////////////////
        Sprite lockLevel = Resources.Load<Sprite>("LockLevel");
        levelButtons = FindObjectsOfType<Button>();
        for (int j = 0; j < 9; j++)
            for (int i = 0; i < levelButtons.Length; i++)
                if (levelButtons[i].name == System.Convert.ToString(j))
                {
                    Debug.Log(PlayerPrefs.GetInt("LastCompleteLevel"));
                    if (System.Convert.ToInt32(levelButtons[i].name) > PlayerPrefs.GetInt("LastCompleteLevel") + 1)
                    {
                        levelButtons[i].image.sprite = lockLevel;
                        levelButtons[i].GetComponent<RectTransform>().localScale = new Vector3(1.4f, 1.3f, 1f);
                        
                        levelButtons[i].enabled = true;
                    }
                    continue;
                }
        //////////////////////LEVEL STARS///////////////////////
        
        Image[] Stars = FindObjectsOfType<Image>();
        Debug.Log(Stars.Length);
        for (int j = 0; j < 9; j++)
            for (int i = 0; i < Stars.Length; i++)
                if (Stars[i].name == "Stars (" + System.Convert.ToString(j + 1) + ")")
                {
                    Debug.Log("=>");
                    if (PlayerPrefs.GetInt("StarAmoungInLevel(" + System.Convert.ToString(j + 1) + ")") == 1)
                    {
                        Sprite OneStar = Resources.LoadAll<Sprite>("StarsPerLevel")[2];

                        Stars[i].sprite = OneStar;

                    }

                    else if (PlayerPrefs.GetInt("StarAmoungInLevel(" + System.Convert.ToString(j + 1) + ")") == 2)
                    {
                        Sprite TwoStars = Resources.LoadAll<Sprite>("StarsPerLevel")[1];
                        Stars[i].sprite = TwoStars;

                    }
                    else if (PlayerPrefs.GetInt("StarAmoungInLevel(" + System.Convert.ToString(j + 1) + ")") == 3)
                    {
                        Sprite ThreeStars = Resources.LoadAll<Sprite>("StarsPerLevel")[0];
                        Stars[i].sprite = ThreeStars;

                    }
                    continue;
                }
        //////////////////////////////////////////////////////
    }
    void Update()
    {
           if(async != null)
            loadingBar.value = async.progress;
        AudioSource[] soundEfects = FindObjectsOfType<AudioSource>();
        for(int i = 0; i < soundEfects.Length; i++)
        {
            if (soundEfects[i].gameObject.name != "hero" && soundEfects[i].gameObject.name != "crystall" && soundEfects[i].gameObject.name != "GameMusic(Clone)")
                soundEfects[i].volume = PlayerPrefs.GetFloat("SFXVolume");
            else if(soundEfects[i].gameObject.name == "hero")
            {
                if (PlayerPrefs.GetFloat("SFXVolume") == 0)
                    soundEfects[i].volume = 0f;
                else soundEfects[i].volume = 0.3f; 
            }
            else if (soundEfects[i].gameObject.name == "crystall")
            {
                if (PlayerPrefs.GetFloat("SFXVolume") == 0)
                    soundEfects[i].volume = 0f;
                else soundEfects[i].volume = 0.3f;
            }
            else if (soundEfects[i].gameObject.name == "GameMusic(Clone)")
            {
                if (PlayerPrefs.GetInt("musicMute") == 0)
                    backgroundMusic.mute = true;
                else backgroundMusic.mute = false;
                soundEfects[i].volume = PlayerPrefs.GetFloat("musicVolume");
            }



        }
    }

    public void loadLevel(int N)
    {
        loadingScreen = Resources.Load<Canvas>("LoadScreen");
        Instantiate(loadingScreen);
        loadingBar = GameObject.Find("LoadingBar").GetComponent<Slider>();
        StartCoroutine(LoadALevel(N + 1));
    }
    public void loadGUI(int N)
    {
        Application.LoadLevel(N);
    }
    public void loadPauseMenu()
    {
        pauseMenu = Resources.Load<Canvas>("Pause");
        if (!GameObject.Find("Pause(Clone)"))
        {
            pauseGame();
           var  newPauseMenu = Instantiate(pauseMenu, transform.position, transform.rotation) as Canvas;
            

        }
    }
    public void loadShop()
    {
        GameObject.Find("LevelMenu").SetActive(false);
        shopMenu = Resources.Load<Canvas>("Shop");
        var newShopMenu = Instantiate(shopMenu, shopMenu.transform.position, shopMenu.transform.rotation) as Canvas;
        money = GameObject.Find("Money").GetComponent<Text>();
        if (money)
            money.text = System.Convert.ToString(PlayerPrefs.GetInt("Money"));
        
        for(int i = 0; i < 10; i++)
        {
            var item = GameObject.Find("Item (" + i + ")");
            if (PlayerPrefs.GetInt(item.name) == 1)
            {
                Destroy(item.transform.FindChild("Price").gameObject);
                item.transform.FindChild("Purshased").gameObject.SetActive(true);
            }
            
        }
    }
    public void closeShop()
    {
        DestroyObject(GameObject.Find("Shop(Clone)"));
        GameObject.Find("Canvas").transform.FindChild("LevelMenu").gameObject.SetActive(true);
    }
    public void continueGame()
    {
        DestroyObject(GameObject.Find("Pause(Clone)"));
        Time.timeScale = 1;
    }
    public void pauseGame()
    {
        Time.timeScale = 0;
    }
    public void loadOptionsMenu()
    {
        
        optionsMenu = Resources.Load<Canvas>("Options");
        continueGame();
        Time.timeScale = 0;
        Instantiate(optionsMenu, transform.position, transform.rotation);
        ///////////////////////SETING SAVED SETTINGS/////////////////////
        soundToggle = GameObject.Find("SoundToggle").GetComponent<Toggle>();
        musicToggle = GameObject.Find("MusicToggle").GetComponent<Toggle>();
        musicSlider = GameObject.Find("MusicSlider").GetComponent<Slider>();
        GFX = GameObject.Find("GFX").GetComponent<Slider>();
        SFX = GameObject.Find("SFX").GetComponent<Slider>();

        if (PlayerPrefs.GetInt("musicMute") == 0)
            musicToggle.isOn = false;
        else musicToggle.isOn = true;

        if (PlayerPrefs.GetInt("soundMute") == 0)
            soundToggle.isOn = false;
        else soundToggle.isOn = true;

        musicSlider.value = PlayerPrefs.GetFloat("musicVolume");
        SFX.value = PlayerPrefs.GetFloat("SFXVolume");
        Debug.Log(PlayerPrefs.GetFloat("SFXVolume"));
        GFX.value = PlayerPrefs.GetInt("GFXQuality");
        /////////////////////////////////////////////////////////////////

    }
    public void closeOptionsMenu()
    {
        DestroyObject(GameObject.Find("Options(Clone)"));
        if (Application.loadedLevel != 0)
            loadPauseMenu();
    }

    public void restartLevel()
    {
        GameObject.FindObjectOfType<GUI>().restartLevel_fromActive();
        
    }
    private void restartLevel_fromActive()
    {
        loadingScreen = Resources.Load<Canvas>("LoadScreen");
        Instantiate(loadingScreen);
        loadingBar = GameObject.Find("LoadingBar").GetComponent<Slider>();
        StartCoroutine(LoadALevel(Application.loadedLevel));
    }
    public void loadNextlevel()
    {
        GameObject.FindObjectOfType<GUI>().loadNextlevel_fromActive();

    }
    private void loadNextlevel_fromActive()
    {
        loadingScreen = Resources.Load<Canvas>("LoadScreen");
        Instantiate(loadingScreen);
        loadingBar = GameObject.Find("LoadingBar").GetComponent<Slider>();
        StartCoroutine(LoadALevel(Application.loadedLevel + 1));
        
    }

    public void GFXSlider()
    {    
        GFX = GameObject.Find("GFX").GetComponent<Slider>();    
        ////////////////////GFX QUALITY/////////////////////////
        PlayerPrefs.SetInt("GFXQuality", (int)GFX.value);
        ////////////////////////////////////////////////////////
        ////////////////////SET GFX QUALITY/////////////////////////
        switch (PlayerPrefs.GetInt("GFXQuality"))
        {
            case 0:
                QualitySettings.currentLevel = QualityLevel.Fastest;
                break;
            case 1:
                QualitySettings.currentLevel = QualityLevel.Fast;
                break;
            case 2:
                QualitySettings.currentLevel = QualityLevel.Simple;
                break;
            case 3:
                QualitySettings.currentLevel = QualityLevel.Good;
                break;
            case 4:
                QualitySettings.currentLevel = QualityLevel.Beautiful;
                break;
            case 5:
                QualitySettings.currentLevel = QualityLevel.Fantastic;
                break;
        }
        ////////////////////////////////////////////////////////
    }
    public void SFXSlider()
    {
        soundToggle = GameObject.Find("SoundToggle").GetComponent<Toggle>();
        SFX = GameObject.Find("SFX").GetComponent<Slider>();
        if (SFX.value == 0f)
            soundToggle.isOn = false;
        else soundToggle.isOn = true;

        //////////////////////SFX MUTE////////////////////////
        if (soundToggle.isOn)
            PlayerPrefs.SetInt("soundMute", 1);
        else PlayerPrefs.SetInt("soundMute", 0);
        ////////////////////////////////////////////////////////
        ////////////////////SFX VOLUME//////////////////////////
        PlayerPrefs.SetFloat("SFXVolume", SFX.value);
        ////////////////////////////////////////////////////////

    }
    public void musicSliderFunc()
    {
        musicToggle = GameObject.Find("MusicToggle").GetComponent<Toggle>();
        musicSlider = GameObject.Find("MusicSlider").GetComponent<Slider>();
        if (musicSlider.value == 0f)
            musicToggle.isOn = false;

        else musicToggle.isOn = true;
        //////////////////////MUSIC MUTE////////////////////////
        if (musicToggle.isOn)
            PlayerPrefs.SetInt("musicMute", 1);
        else PlayerPrefs.SetInt("musicMute", 0);
        ////////////////////////////////////////////////////////
        ////////////////////MUSIC VOLUME////////////////////////
        PlayerPrefs.SetFloat("musicVolume", musicSlider.value);
        ////////////////////////////////////////////////////////
    }
    public void MusicToggle()
    {
       
        musicToggle = GameObject.Find("MusicToggle").GetComponent<Toggle>();
        musicSlider = GameObject.Find("MusicSlider").GetComponent<Slider>();

        if (musicToggle.isOn)
        {
            if (musicSlider.value == 0)
                musicSlider.value = 0.1f;
            
        }
        else musicSlider.value = 0f;
        //////////////////////MUSIC MUTE////////////////////////
        if (musicToggle.isOn)
            PlayerPrefs.SetInt("musicMute", 1);
        else PlayerPrefs.SetInt("musicMute", 0);
        ////////////////////////////////////////////////////////
        ////////////////////MUSIC VOLUME////////////////////////
        PlayerPrefs.SetFloat("musicVolume", musicSlider.value);
        ////////////////////////////////////////////////////////


    }
    public void SoundToggle()
    {

        soundToggle = GameObject.Find("SoundToggle").GetComponent<Toggle>();
        SFX = GameObject.Find("SFX").GetComponent<Slider>();
        if (soundToggle.isOn) {
            if (SFX.value == 0)
                SFX.value = 0.1f;
        }
        else SFX.value = 0f;
        //////////////////////SFX MUTE////////////////////////
        if (soundToggle.isOn)
            PlayerPrefs.SetInt("soundMute", 1);
        else PlayerPrefs.SetInt("soundMute", 0);
        ////////////////////////////////////////////////////////
        //////////////////////SFX VOLUME//////////////////////////
        PlayerPrefs.SetFloat("SFXVolume", SFX.value);
        //////////////////////////////////////////////////////////
    }

    private IEnumerator LoadALevel(int index)
    {
        async = Application.LoadLevelAsync(index);
        yield return async;
        
    }
    private int r, g, b;
    private GameObject item;
    private string product;
     
    public void getR(int R)
    {
        r = R;
    }
    public void getG(int G)
    {
        g = G;
    }
    public void getB(int B)
    {
        b = B;
    }
    public void buyArmor()
    {
        if (PlayerPrefs.GetInt("Money") > 85)
        {
            product = "armor";
            var confirmMenu = Instantiate(Resources.Load("ConfirmMenu")) as Canvas;
        item = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.transform.parent.gameObject;
        }
    }
    public void buyBullet()
    {
          
        if (PlayerPrefs.GetInt("Money") > 55)
        {
            product = "plasma";
            var confirmMenu = Instantiate(Resources.Load("ConfirmMenu")) as Canvas;
            item = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.transform.parent.gameObject;
        }
    }
    public void Confirm(bool confirm)
    {
        if (confirm)
        {
            Destroy(item.transform.FindChild("Price").gameObject);
            item.transform.FindChild("Purshased").gameObject.SetActive(true);
            if(product == "armor")
            {
                PlayerPrefs.SetFloat("ArmorR", r);
                PlayerPrefs.SetFloat("ArmorG", g);
                PlayerPrefs.SetFloat("ArmorB", b);
                PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") - 85);
            }
            else if(product == "plasma")
            {
                PlayerPrefs.SetFloat("BulletColorR", r);
                PlayerPrefs.SetFloat("BulletColorG", g);
                PlayerPrefs.SetFloat("BulletColorB", b);
                PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") - 55);
            } 
            PlayerPrefs.SetInt(item.name, 1);
            money = GameObject.Find("Money").GetComponent<Text>();
            if (money)
                money.text = System.Convert.ToString(PlayerPrefs.GetInt("Money"));

            Destroy(GameObject.Find("ConfirmMenu(Clone)"));
        }
        else Destroy(GameObject.Find("ConfirmMenu(Clone)"));

    }
    public void dressArmor()
    {
        PlayerPrefs.SetFloat("ArmorR", r);
        PlayerPrefs.SetFloat("ArmorG", g);
        PlayerPrefs.SetFloat("ArmorB", b);
    }
    public void usePlasma()
    {
        PlayerPrefs.SetFloat("BulletColorR", r);
        PlayerPrefs.SetFloat("BulletColorG", g);
        PlayerPrefs.SetFloat("BulletColorB", b);
    }

}
