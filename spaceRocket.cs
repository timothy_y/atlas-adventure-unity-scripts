﻿using UnityEngine;
using System.Collections;

public class spaceRocket : MonoBehaviour
{
    public float maxSpeed = 10; 
    public bool launch = false;
    private GameObject hero;
    private bool written = false;
    // Use this for initialization
    void Start()
    {
        hero = GameObject.Find("hero");
    }

    // Update is called once per frame
    void Update()
    {
        
        if (transform.position.x - hero.gameObject.transform.position.x < 10f && !written)
        {
            GameSubtitles subtitles = GameObject.Find("Subtitles").GetComponent<GameSubtitles>();
            StartCoroutine(subtitles.printSubtitle("Oh, it seems like this is the rocket.", 0f));
            StartCoroutine(subtitles.printSubtitle("So i can to fly off of this crazy planet.", 2f));
            StartCoroutine(subtitles.printSubtitle("", 5f));
            written = true;
        }
        if (launch)
        {     
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, maxSpeed));
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, maxSpeed);            
        }
        

    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        characterController hero = collider.GetComponent<characterController>();
       
        if (hero)
        {
            hero.GetComponent<SpriteRenderer>().enabled = false;
            launch = true;
            gameObject.GetComponent<Animator>().enabled = true;
            gameObject.GetComponent<AudioSource>().enabled = true;
        }
    }
}