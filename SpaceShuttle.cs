﻿using UnityEngine;
using System.Collections;

public class SpaceShuttle : MonoBehaviour {

    public float maxSpeed = 5f;
    public bool moveStatus = true;
   
    void Start()
    {

    }

    void Update()
    {
        
            if (moveStatus)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, maxSpeed));
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, maxSpeed);
            }
       
    }
    public void OnTriggerEnter2D(Collider2D collider)
    {
        GameObject hero = GameObject.Find("hero");
        if ((collider.gameObject.name == "stopCollider"))
        { 
            moveStatus = false;
            maxSpeed = 0f;
            GetComponent<Rigidbody2D>().isKinematic = false;
            hero.GetComponent<characterController>().maxSpeed = 10f;
            hero.GetComponent<characterController>().jumpForce = 500f;
            hero.GetComponent<SpriteRenderer>().enabled = true;
            hero.GetComponent<characterController>().recharged = true;
        }
           
       
    }
}
