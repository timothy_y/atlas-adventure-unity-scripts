﻿using UnityEngine;
using System.Collections;

public class BossSupportRobot : Enemy {

    private int damage = 0;
    public float rate = 3.0f;
    public GameObject aim;
    private GameObject laser;
    public float maxSpeed = 3f;
    public bool facingRight = true;
    public bool moveStatus = true;
    private int moveKoeff = 1;


    void Awake()
    {
        laser = Resources.Load<GameObject>("LongLaser");

    }
    // Use this for initialization
    void Start()
    {

        InvokeRepeating("Shoot", rate, rate);
    }

    // Update is called once per frame
    void Update()
    {
        if (moveStatus)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(maxSpeed, 0f));
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveKoeff * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            Flip();
            moveStatus = true;

        }
        if ((aim.transform.position.x - transform.position.x) > 0)
        {
            facingRight = false;
            Vector3 theScale = transform.localScale;
            theScale.x = -1;
            transform.localScale = theScale;
        }
        else
        {
            facingRight = true;
            Vector3 theScale = transform.localScale;
            theScale.x = 1;
            transform.localScale = theScale;
        }

    }

    public override void recieveDamage()
    {
        damage++;
        if (damage == 7)
        {

            die();

        }
    }
    private void Shoot()
    {
        float deltaX = aim.transform.position.x - transform.position.x;
        float deltaY = aim.transform.position.y - transform.position.y;
        Quaternion rotation = Quaternion.Euler(0, 0, Mathf.Rad2Deg * Mathf.Atan(deltaY / deltaX));

        Vector3 position = transform.position;
        var newLaser = Instantiate(laser, position, rotation) as GameObject;
        newLaser.GetComponent<LongLaser>().parent = gameObject;
        // newLaser.transform.localScale = new Vector3(Mathf.Sqrt(Mathf.Pow(deltaX, 2) + Mathf.Pow(deltaY, 2))*6.5f, 1, 1);
        newLaser.transform.position = new Vector3(newLaser.transform.position.x, newLaser.transform.position.y, -9);
        newLaser.GetComponent<LongLaser>().deltaX = deltaX;
        newLaser.GetComponent<LongLaser>().deltaY = deltaY;
        newLaser.GetComponent<LongLaser>().direction = facingRight;
        newLaser.GetComponent<LongLaser>().time = Time.time;
        

    }
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (unit && unit is characterController)
        {
            unit.recieveDamage();
            if ((unit.transform.position.x - transform.position.x) < 0)
                unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
            else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);

        }
        if (laser)
            recieveDamage();

        if ((collider.gameObject.name == "stopCollaiderLeft"))
        {
            moveStatus = false;
            moveKoeff = -1;
        }
        if ((collider.gameObject.name == "stopCollaiderRight"))
        {
            moveStatus = false;
            moveKoeff = 1;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
