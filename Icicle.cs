﻿using UnityEngine;
using System.Collections;

public class Icicle : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if(!GameObject.Find("IceBoss"))
            if (Mathf.Abs(GameObject.Find("hero").transform.position.x - gameObject.transform.position.x) < 10f)
            {
                GetComponent<Animator>().enabled = true;
                DestroyObject(gameObject, 5f);
            }
    }
}
