﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

    public Door nextDoor;
    public bool inDoor = true;
    public bool outDoor = false;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Vector3 position = nextDoor.transform.position;
        position.z = -9f;
        characterController hero = collider.GetComponent<characterController>();
        if (hero && hero.move > 0 && inDoor)
            hero.transform.position = position;
        else if (hero && hero.move < 0 && outDoor)
            hero.transform.position = position;
    }
}
