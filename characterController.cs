﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class characterController : Unit
{

    public float maxSpeed = 75f;
    public float jumpForce = 200f;
    public float rate = 1f;
    public int MaxScore = 28;
    public bool facingRight = true;
    public bool grounded = true;
    public bool recharged = true;
    public float spawnX, spawnY;
    public float move;
    public float energyReloadTime = 3f;
    public bool jumpPossibility = false;
    public  int jumpCounter = 1;
    public int score = 0;
    public int lives = 3;
    public Animator animator;
    public int shootDirection = -1;
    public TextScript[] TextFields;
    public float levelTime;  
    private float time;
    public Slider livesBar;
    public Slider Energy;
    private float rechargeTime;
    private bool invulnerability = false;


    public state state
    {
        get { return (state)animator.GetInteger("state"); }
        set { animator.SetInteger("state", (int)value); }
    }
    private float startTouchPosition, endTouchPosition;
    private Laser laser;

    private void Awake()
    {
        Energy = GameObject.Find("EnergyBar").GetComponent<Slider>();
        laser = Resources.Load<Laser>("Impulse");
        Time.timeScale = 1;
       
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log(Application.loadedLevel);
        spawnX = transform.position.x;
        spawnY = transform.position.y;
        GetComponent<SpriteRenderer>().color = new Color(PlayerPrefs.GetFloat("ArmorR")/255, PlayerPrefs.GetFloat("ArmorG")/255, PlayerPrefs.GetFloat("ArmorB")/255, 1);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        TextFields = Object.FindObjectsOfType<TextScript>();
        foreach (TextScript i in TextFields)
            i.MaxScore = MaxScore;
        levelTime = Time.realtimeSinceStartup;
        foreach (TextScript i in TextFields)
            i.score = score;
        checkGround();
        if (grounded)
        {
            jumpPossibility = true;
            jumpCounter = 0;
        }
        if (jumpCounter >= 2)
            jumpPossibility = false;

    }


    void Update()
    {
        if (!recharged)
            Recharge();
#if UNITY_EDITOR
        int scaleUnit = 1;
        if (Input.GetAxis("Horizontal") > 0)
            scaleUnit = 1;
        else scaleUnit = -1;
        if (Input.GetButtonDown("Horizontal"))
            Hero_move(scaleUnit);
        if (Input.GetButtonUp("Horizontal"))
            Hero_move(0);
        if (Input.GetButtonDown("Jump"))
            Hero_jump();
        if (Input.GetButtonDown("Fire3"))
            Hero_shoot();
#endif
        //#if UNITY_EDITOR

        //        Canvas[] canvases = Object.FindObjectsOfType<Canvas>();
        //        foreach(Canvas i in canvases)
        //        i.scaleFactor = 0.3f;
        //#endif


        if (move == 0 && jumpCounter == 0)
        {
            gameObject.GetComponent<AudioSource>().enabled = false;
            state = state.idle;
        }
       GetComponent<Rigidbody2D>().velocity = new Vector2(move * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
       if (move > 0 && !facingRight)
           Flip();
       else if (move < 0 && facingRight)
           Flip();
        if (lives <= 0)
            Application.LoadLevel(Application.loadedLevelName);

    }
   private void Recharge()
    {   
        if (Time.time - rechargeTime >= energyReloadTime)
            recharged = true;
        else
        {
            Energy.value += Time.deltaTime;
            recharged = false;
        }
    }
   public void Hero_shoot()
    {
        if(!GameObject.Find("Pause(Clone)") && !GameObject.Find("Options(Clone)"))
            if (recharged) {
                Vector3 position = transform.position;
                var newLaser = Instantiate(laser, position, gameObject.transform.rotation) as Laser;
                if (facingRight)
                    newLaser.direction = 1f;
                else newLaser.direction = -1f;
                newLaser.parent = gameObject;
                newLaser.destroyTime = 1f;
                newLaser.sprite.color = new Color(PlayerPrefs.GetFloat("BulletColorR")/255, PlayerPrefs.GetFloat("BulletColorG")/255, PlayerPrefs.GetFloat("BulletColorB")/255, 1);
                Energy.value = 0;
                recharged = false;
                rechargeTime = Time.time;
            }
        
    }
   public void Hero_jump()
    {
        if (!GameObject.Find("Pause(Clone)") && !GameObject.Find("Options(Clone)"))
            if (jumpPossibility)
            {
                gameObject.GetComponent<AudioSource>().enabled = true;
                jumpCounter++;
                state = state.jump;
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));
                
            }
    }
    public override void recieveDamage()
    {
        livesBar = GameObject.Find("LiveBar").GetComponent<Slider>();
        if ((Time.time - time) < 0.3f)
            invulnerability = true;
        else invulnerability = false;
        if (!invulnerability)
            lives--;
        time = Time.time;
        livesBar.value = lives;
        GetComponent<Rigidbody2D>().velocity = Vector3.zero;
       
       

       // GetComponent<Rigidbody2D>().AddForce(transform.up * 10f, ForceMode2D.Impulse);
       
        //GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);

    }
    public void Hero_move(int direction)
    {
        if (!GameObject.Find("Pause(Clone)") && !GameObject.Find("Options(Clone)"))
        {
            gameObject.GetComponent<AudioSource>().enabled = true;
            move = direction;
            if (jumpCounter == 0)
                state = state.move;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
    }
    public void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    void OnTriggerEnter2D(Collider2D collider)
    {

        if ((collider.gameObject.name == "DieZone"))
        {
            lives--;
            livesBar = GameObject.Find("LiveBar").GetComponent<Slider>();
            time = Time.time;
            livesBar.value = lives;
            transform.position = new Vector3(spawnX, spawnY, transform.position.z);
        }
        if (collider.gameObject.name == "crystall")
        {
           
            collider.gameObject.GetComponent<AudioSource>().enabled = true;
            collider.gameObject.GetComponent<PolygonCollider2D>().enabled = false;
            score++;
            Destroy(collider.gameObject, 0.2f);
        }

        if ((collider.gameObject.name == "ExitDoor"))
        {
            foreach (TextScript i in TextFields)
                i.levelTime = levelTime;
            Canvas ScoreMenu = Resources.Load<Canvas>("ScoreMenu");
            if (!GameObject.Find("ScoreMenu(Clone)"))
            {

                Instantiate(ScoreMenu, transform.position, transform.rotation);
                if ((float)score / MaxScore < 0.33)
                {
                    DestroyObject(GameObject.Find("Star" + 3));
                    DestroyObject(GameObject.Find("Star" + 2));
                    PlayerPrefs.SetInt("StarAmoungInLevel(1)", 1);
                }
                else if ((float)score / MaxScore > 0.33 && (float)score / MaxScore < 0.66)
                {
                    PlayerPrefs.SetInt("StarAmoungInLevel(1)", 2);
                    DestroyObject(GameObject.Find("Star" + 3));
                }
                else PlayerPrefs.SetInt("StarAmoungInLevel(1)", 3);
            }
            DestroyObject(gameObject, 0.2f);
            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + score);
            if ((Application.loadedLevel - 1) > PlayerPrefs.GetInt("LastCompleteLevel"))
                PlayerPrefs.SetInt("LastCompleteLevel", 1);

        }
        if ((collider.gameObject.name == "spaceRocket"))
        {
            foreach (TextScript i in TextFields)
                i.levelTime = levelTime;
            Canvas ScoreMenu = Resources.Load<Canvas>("ScoreMenu");
            if (!GameObject.Find("ScoreMenu(Clone)"))
            {

                Instantiate(ScoreMenu, transform.position, transform.rotation);
                if ((float)score / MaxScore < 0.33)
                {
                    DestroyObject(GameObject.Find("Star" + 3));
                    DestroyObject(GameObject.Find("Star" + 2));
                    PlayerPrefs.SetInt("StarAmoungInLevel(2)", 1);
                }
                else if ((float)score / MaxScore > 0.33 && (float)score / MaxScore < 0.66)
                {
                    PlayerPrefs.SetInt("StarAmoungInLevel(2)", 2);
                    DestroyObject(GameObject.Find("Star" + 3));
                }
                else PlayerPrefs.SetInt("StarAmoungInLevel(2)", 3);
            }
            DestroyObject(gameObject, 0.2f);
            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") +  score);
            if ((Application.loadedLevel - 1) > PlayerPrefs.GetInt("LastCompleteLevel"))
                PlayerPrefs.SetInt("LastCompleteLevel", 2);
        }
        if ((collider.gameObject.name == "Portal(Clone)"))
        {
            foreach (TextScript i in TextFields)
                i.levelTime = levelTime;
            Canvas ScoreMenu = Resources.Load<Canvas>("ScoreMenu");
            if (!GameObject.Find("ScoreMenu(Clone)"))
            {

                Instantiate(ScoreMenu, transform.position, transform.rotation);
                if ((float)score / MaxScore < 0.33)
                {
                    DestroyObject(GameObject.Find("Star" + 3));
                    DestroyObject(GameObject.Find("Star" + 2));
                    PlayerPrefs.SetInt("StarAmoungInLevel(3)", 1);
                }
                else if ((float)score / MaxScore > 0.33 && (float)score / MaxScore < 0.66)
                {
                    PlayerPrefs.SetInt("StarAmoungInLevel(3)", 2);
                    DestroyObject(GameObject.Find("Star" + 3));
                }
                else PlayerPrefs.SetInt("StarAmoungInLevel(3)", 3);
            }
            DestroyObject(gameObject, 0.2f);
            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + score);
            if ((Application.loadedLevel - 1) > PlayerPrefs.GetInt("LastCompleteLevel"))
                PlayerPrefs.SetInt("LastCompleteLevel", 3);

        }
        
        if ((collider.gameObject.name == "SpaceShip"))
        {
            foreach (TextScript i in TextFields)
                i.levelTime = levelTime;
            Canvas ScoreMenu = Resources.Load<Canvas>("ScoreMenu");
            if (!GameObject.Find("ScoreMenu(Clone)"))
            {

                Instantiate(ScoreMenu, transform.position, transform.rotation);
                if ((float)score / MaxScore < 0.33)
                {
                    DestroyObject(GameObject.Find("Star" + 3));
                    DestroyObject(GameObject.Find("Star" + 2));
                    PlayerPrefs.SetInt("StarAmoungInLevel(6)", 1);
                }
                else if ((float)score / MaxScore > 0.33 && (float)score / MaxScore < 0.66)
                {
                    PlayerPrefs.SetInt("StarAmoungInLevel(6)", 2);
                    DestroyObject(GameObject.Find("Star" + 3));
                }
                else PlayerPrefs.SetInt("StarAmoungInLevel(6)", 3);
            }
            DestroyObject(gameObject, 0.2f);
            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + score);
            if ((Application.loadedLevel - 1) > PlayerPrefs.GetInt("LastCompleteLevel"))
                PlayerPrefs.SetInt("LastCompleteLevel", 6);
        }
        if ((collider.gameObject.name == "ControlPanel"))
        {
            foreach (TextScript i in TextFields)
                i.levelTime = levelTime;
            Canvas ScoreMenu = Resources.Load<Canvas>("ScoreMenu");
            if (!GameObject.Find("ScoreMenu(Clone)"))
            {

                Instantiate(ScoreMenu, transform.position, transform.rotation);
                if ((float)score / MaxScore < 0.33)
                {
                    DestroyObject(GameObject.Find("Star" + 3));
                    DestroyObject(GameObject.Find("Star" + 2));
                    PlayerPrefs.SetInt("StarAmoungInLevel(7)", 1);
                }
                else if ((float)score / MaxScore > 0.33 && (float)score / MaxScore < 0.66)
                {
                    PlayerPrefs.SetInt("StarAmoungInLevel(7)", 2);
                    DestroyObject(GameObject.Find("Star" + 3));
                }
                else PlayerPrefs.SetInt("StarAmoungInLevel(7)", 3);
            }
            DestroyObject(gameObject, 0.2f);
            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + score);
            if ((Application.loadedLevel - 1) > PlayerPrefs.GetInt("LastCompleteLevel"))
                PlayerPrefs.SetInt("LastCompleteLevel", 7);
        }
        if ((collider.gameObject.name == "IceCaveEntrance"))
        {
            foreach (TextScript i in TextFields)
                i.levelTime = levelTime;
            Canvas ScoreMenu = Resources.Load<Canvas>("ScoreMenu");
            if (!GameObject.Find("ScoreMenu(Clone)"))
            {

                Instantiate(ScoreMenu, transform.position, transform.rotation);
                if ((float)score / MaxScore < 0.33)
                {
                    DestroyObject(GameObject.Find("Star" + 3));
                    DestroyObject(GameObject.Find("Star" + 2));
                    PlayerPrefs.SetInt("StarAmoungInLevel(4)", 1);
                }
                else if ((float)score / MaxScore > 0.33 && (float)score / MaxScore < 0.66)
                {
                    PlayerPrefs.SetInt("StarAmoungInLevel(4)", 2);
                    DestroyObject(GameObject.Find("Star" + 3));
                }
                else PlayerPrefs.SetInt("StarAmoungInLevel(4)", 3);
            }
            DestroyObject(gameObject, 0.2f);
            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + score);
            if ((Application.loadedLevel - 1) > PlayerPrefs.GetInt("LastCompleteLevel"))
                PlayerPrefs.SetInt("LastCompleteLevel", 4);
        }
        if ((collider.gameObject.name == "CaveExit"))
        {
            foreach (TextScript i in TextFields)
                i.levelTime = levelTime;
            Canvas ScoreMenu = Resources.Load<Canvas>("ScoreMenu");
            if (!GameObject.Find("ScoreMenu(Clone)"))
            {

                Instantiate(ScoreMenu, transform.position, transform.rotation);
                if ((float)score / MaxScore < 0.33)
                {
                    DestroyObject(GameObject.Find("Star" + 3));
                    DestroyObject(GameObject.Find("Star" + 2));
                    PlayerPrefs.SetInt("StarAmoungInLevel(5)", 1);
                }
                else if ((float)score / MaxScore > 0.33 && (float)score / MaxScore < 0.66)
                {
                    PlayerPrefs.SetInt("StarAmoungInLevel(5)", 2);
                    DestroyObject(GameObject.Find("Star" + 3));
                }
                else PlayerPrefs.SetInt("StarAmoungInLevel(5)", 3);
            }
            DestroyObject(gameObject, 0.2f);
            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + score);
            if ((Application.loadedLevel - 1) > PlayerPrefs.GetInt("LastCompleteLevel"))
                PlayerPrefs.SetInt("LastCompleteLevel", 5);
        }
    }   

    public void checkGround()
    {
        Vector3 r = transform.position;
        r.y -= 1f;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(r, 0.3f);
        grounded = colliders.Length > 1;
    }

}
public enum state
{
    idle,
    move,
    jump
};