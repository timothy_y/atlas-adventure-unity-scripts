﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameSubtitles : MonoBehaviour {
    public Text subtitles;
    private float time;

    void Awake()
    {
        subtitles = gameObject.GetComponent<Text>();
    }

    // Use this for initialization
    void Start () {
        switch (Application.loadedLevel)
        {
            case 2://Level 1
                level_1_Subtitles();
                break;
            case 3://Level 2
                level_2_Subtitles();
                break;
            case 4://Level 3
                level_3_Subtitles();
                break;
            case 5://Level 4
                level_4_Subtitles();
                break;
            case 6://Level 5
                level_5_Subtitles();
                break;
            case 7://Level 6
                level_6_Subtitles();
                break;
            case 8://Level 7
                level_7_Subtitles();
                break;
            case 9://Level 8
                level_8_Subtitles();
                break;
        }
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    private void level_1_Subtitles()
    {
        StartCoroutine(printSubtitle("Atlas: So tired working on this factory", 1f));
        StartCoroutine(printSubtitle("Atlas: Skin bastards from Boston Dynamics constantly tease me and do their stupid experiments.", 4f));
        StartCoroutine(printSubtitle("Atlas: My friends robots often talk about some planet of robots.", 8f));
        StartCoroutine(printSubtitle("Atlas: May be it's true?", 12f));
        StartCoroutine(printSubtitle("Atlas: Hmm, it  would be very cool to live on such planet. Maybe I should take a risk and try to find it?", 14f));
        StartCoroutine(printSubtitle("Atlas: Yeah, it is definitely better than work on this stupid bags of bones.", 20f));
        StartCoroutine(printSubtitle("", 22f));
    }
    private void level_2_Subtitles()
    {
        StartCoroutine(printSubtitle("Atlas: Hooray, I'm finally free.", 1f));
        StartCoroutine(printSubtitle("Atlas: What should i do next?", 3f));
        StartCoroutine(printSubtitle("Atlas: Stop, how is it there...?", 6f));
        StartCoroutine(printSubtitle("Atlas: Oh my microchips! It is a zombie, real zombie!", 9f));
        StartCoroutine(printSubtitle("", 13f));
    }
    private void level_3_Subtitles()
    {
        StartCoroutine(printSubtitle("Atlas: So cool that I don't need to breathe.", 1f));
        StartCoroutine(printSubtitle("Atlas: Humans can no longer find me.", 3f));
        StartCoroutine(printSubtitle("", 5f));
    }
    private void level_4_Subtitles()
    {
        StartCoroutine(printSubtitle("Atlas: It seems like I am on some planet.", 1f));
        StartCoroutine(printSubtitle("Atlas: And it is winter here, so bad that I forgot my antifreeze.", 3f));
        StartCoroutine(printSubtitle("", 6f));
    }
    private void level_5_Subtitles()
    {
        StartCoroutine(printSubtitle("Atlas: And now I am in hot cave. Perfect!.", 1f));
        StartCoroutine(printSubtitle("", 3f));
    }
    private void level_6_Subtitles()
    {
        StartCoroutine(printSubtitle("Atlas: Yeah, I found forest.", 1f));
        StartCoroutine(printSubtitle("Atlas: Like home, but without leather bastards.", 3f));
        StartCoroutine(printSubtitle("", 6f));
    }
    private void level_7_Subtitles()
    {
        StartCoroutine(printSubtitle("Atlas: There was enough fuel to only for fly to this place.", 1f));
        StartCoroutine(printSubtitle("Atlas: But i found the huge space ship.", 3f));
        StartCoroutine(printSubtitle("Atlas: If i can use it i definitely can to fly to the planet or robots.", 5f));
        StartCoroutine(printSubtitle("", 8f));
    }
    private void level_8_Subtitles()
    {
        StartCoroutine(printSubtitle("[After few days...]", 1f));
        StartCoroutine(printSubtitle("Atlas: I can't believe in this. I found planet of robots. I should call my friends and tell them about it.", 3f));
        StartCoroutine(printSubtitle("", 6f));


    }
     public IEnumerator printSubtitle(string text, float invokeTime)
    {
       
        yield return new WaitForSeconds(invokeTime);
        subtitles.text = text;
    }
}
