﻿using UnityEngine;
using System.Collections;

public class moveEnemy : Enemy {

    public float maxSpeed = 10f;
    bool facingRight = true;
    public bool moveStatus = true;
    private Collider2D col;
    private int moveKoeff = 1;
    

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (moveStatus)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(maxSpeed, 0f));
            GetComponent<Rigidbody2D>().velocity = new Vector2( moveKoeff * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            Flip();
            moveStatus = true;
           
        }
       
        

    }
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        Laser laser = collider.GetComponent<Laser>();
        if (unit && unit is characterController)
        {
            if (Mathf.Abs(unit.transform.position.y - transform.position.y) > 1.2f && Mathf.Abs(unit.transform.position.x - transform.position.x) < GetComponent<Renderer>().bounds.size.y)
            {
                unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(2000, 1000), ForceMode2D.Force);
                recieveDamage();
            }  
            else
            {
                unit.recieveDamage();
                if ((unit.transform.position.x - transform.position.x) < 0)
                    unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(-1500, 0), ForceMode2D.Force);
                else unit.GetComponent<Rigidbody2D>().AddForce(new Vector3(1500, 0), ForceMode2D.Force);
                
            }
            
        }
        if (laser)
            recieveDamage();


        if ((collider.gameObject.name == "stopCollaiderLeft"))
        {
            moveStatus = false;
            moveKoeff = -1;
        }
        if ((collider.gameObject.name == "stopCollaiderRight"))
        {
            moveStatus = false;
            moveKoeff = 1;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    
}
